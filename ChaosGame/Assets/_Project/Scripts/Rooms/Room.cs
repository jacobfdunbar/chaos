using UnityEngine;

public class Room : MonoBehaviour
{
    [SerializeField] private DoorAnimator.AnimationType animationOverride = DoorAnimator.AnimationType.Default;
    public DoorAnimator.AnimationType AnimationOverride => animationOverride;

    public Door EntranceDoor { get; set; }
}