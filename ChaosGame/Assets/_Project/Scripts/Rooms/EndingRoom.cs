using UnityEngine;
using UnityEngine.Events;

public class EndingRoom : Room
{
    [SerializeField] private UnityEvent onEndTriggered;

    private bool _hasEnded = false;
    
    public void TriggerEnd()
    {
        if (_hasEnded) return;
        _hasEnded = true;
        EntranceDoor.Close();
        onEndTriggered?.Invoke();
    }
}