using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class InteractionText : MonoBehaviour
{
    [SerializeField] private InteractableVariable interactable = null;

    private TMP_Text _text;
    
    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
        _text.text = string.Empty;
    }

    private void OnEnable()
    {
        interactable.AddListener(UpdateInteractable);
    }

    private void OnDisable()
    {
        interactable.RemoveListener(UpdateInteractable);
    }

    private void UpdateInteractable()
    {
        if (interactable.Value == null && !string.IsNullOrEmpty(_text.text))
        {
            _text.text = string.Empty;
        } else if (interactable.Value != null)
        {
            _text.text = interactable.Value.PromptMessage;
        }
    }
}