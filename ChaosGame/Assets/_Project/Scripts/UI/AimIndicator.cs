﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AimIndicator : MonoBehaviour
{
    [SerializeField] private InteractableVariable interactable = null;
    
    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        interactable.AddListener(UpdateVisuals);
    }

    private void OnDisable()
    {
        interactable.RemoveListener(UpdateVisuals);
    }

    private void UpdateVisuals()
    {
        _image.DOKill();
        if (interactable.Value == null)
        {
            _image.DOFade(0f, 0.1f);
        }
        else
        {
            _image.DOFade(1f, 0.05f);
        }
    }
}