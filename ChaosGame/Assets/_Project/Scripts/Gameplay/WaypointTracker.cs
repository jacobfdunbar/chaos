using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WaypointTracker : MonoBehaviour
{
    [SerializeField] private UnityEvent onCompleted = default;

    private List<WaypointTrigger> _waypoints;
    private readonly Queue<WaypointTrigger> _hitWaypoints = new Queue<WaypointTrigger>();
    private bool _hasBeenCompleted = false;
    
    private void Awake()
    {
        _waypoints = GetComponentsInChildren<WaypointTrigger>().ToList();
        for (var i = 0; i < _waypoints.Count; i++)
        {
            _hitWaypoints.Enqueue(null);   
        }
    }

    public void WaypointHit(WaypointTrigger trigger)
    {
        if (_hasBeenCompleted) return;
        _hitWaypoints.Dequeue();
        _hitWaypoints.Enqueue(trigger);

        var currentHits = _hitWaypoints.ToList();
        if (_waypoints.Where((t, i) => t != currentHits[i]).Any()) return;
        _hasBeenCompleted = true;
        onCompleted?.Invoke();
    }
}