using Toolkit.Gameplay.Collisions;
using UnityEngine;

public class WaypointTrigger : Triggerable
{
    private WaypointTracker _tracker;

    private void Awake()
    {
        _tracker = GetComponentInParent<WaypointTracker>();
    }

    protected override void OnEnter(Collider other)
    {
        _tracker.WaypointHit(this);
    }
}