using Toolkit.Variables;
using UnityEngine;

[CreateAssetMenu(fileName = "New InteractableVariable", menuName = "Variables/Interactable")]
public class InteractableVariable : DefaultedVariable<Interactable>
{
    public void OnLookBegin()
    {
        if (!Value) return;
        Value.OnLookBegin();
    }

    public void OnLookStay()
    {
        if (!Value) return;
        Value.OnLookStay();
    }

    public void OnLookEnd()
    {
        if (!Value) return;
        Value.OnLookEnd();
    }

    public void OnInteract(bool left)
    {
        if (!Value) return;
        Value.OnInteract(left);
    }
}