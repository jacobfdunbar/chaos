﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Interactable : MonoBehaviour
{
    [SerializeField] private string promptMessage = "";
    public string PromptMessage => promptMessage;

    public virtual void OnLookBegin() {}
    public virtual void OnLookStay() {}
    public virtual void OnLookEnd() {}
    public virtual void OnInteract(bool left) {}
}