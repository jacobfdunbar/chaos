﻿using UnityEngine;

public class Box : Interactable
{
    public override void OnLookStay()
    {
        transform.Rotate(Vector3.one * (Time.deltaTime * 90f));
    }
}
