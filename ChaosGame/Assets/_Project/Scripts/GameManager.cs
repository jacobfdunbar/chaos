﻿using System.Collections;
using Toolkit.Utilities;
using UnityEngine.SceneManagement;

public class GameManager : SingletonBehaviour<GameManager>
{
    private bool _isGameOver = false;
    
    public void OnGameOver()
    {
        if (_isGameOver) return;
        _isGameOver = true;
        StartCoroutine(EndGame());

        static IEnumerator EndGame()
        {
            yield return Wait.Time(6f);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}