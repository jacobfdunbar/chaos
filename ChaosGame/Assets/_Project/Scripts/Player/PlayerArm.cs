using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(DOTweenAnimation))]
public class PlayerArm : MonoBehaviour
{
    [SerializeField] private PlayerInputController input = null;
    [SerializeField] private InteractableVariable interactable = null;
    [SerializeField] private bool isLeftArm = true;

    private DOTweenAnimation _animation;
    
    private void Awake()
    {
        _animation = GetComponent<DOTweenAnimation>();
    }

    private void OnEnable()
    {
        input.Interact += OnInteract;
    }

    private void OnDisable()
    {
        input.Interact -= OnInteract;
    }

    private void OnInteract(bool left)
    {
        if (left != isLeftArm || interactable.Value == null) return;
        _animation.DORestart();
    }
}