﻿using Toolkit.Variables;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraSettings", menuName = "Settings/Camera")]
public class CameraSettings : DataObject
{
    [Header("Camera Movement")]
    public float Sensitivity = 2f;
    public float Smoothing = 1f;

    [Header("Interactables")]
    public float InteractDistance = 2f;
    public LayerMask InteractableLayers;
}
