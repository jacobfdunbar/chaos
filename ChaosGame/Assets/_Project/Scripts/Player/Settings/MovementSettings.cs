﻿using System;
using Toolkit.Variables;
using UnityEngine;

[CreateAssetMenu(fileName = "MovementSettings", menuName = "Settings/Movement")]
public class MovementSettings : DataObject
{
    [Header("Movement")]
    public float WalkSpeed = 5f;
    public float RunSpeed = 9f;
    public float RunBuildUpTime = 0.25f;

    public float SlopeForce = 5f;
    public float SlopeForceRayLength = 1.5f;

    public float SpeedDifference => Mathf.Abs(WalkSpeed - RunSpeed);

    [Header("Jumping")]
    public bool CanJump = false;
    public AnimationCurve JumpFallOff = default;
    public float JumpMultiplier = 10;
}
