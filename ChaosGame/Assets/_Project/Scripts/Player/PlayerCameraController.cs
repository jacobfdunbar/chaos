﻿using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    [Header("Input")]
    [SerializeField] private PlayerInputController input = null;
    
    [Header("Camera")]
    [SerializeField] private CameraSettings _settings = default;
    [SerializeField] private Transform playerTransform = null;
    
    [Header("Interactables")]
    [SerializeField] private InteractableVariable lookedAtInteractable = null;

    private Camera _playerCamera;
    private Vector2 _smoothScale;
    
    private Vector2 _mouseLook;
    private Vector2 _smooth;

    private void Awake()
    {
        _playerCamera = Camera.main;
        var scale = _settings.Sensitivity * _settings.Smoothing;
        _smoothScale = new Vector2(scale, scale);

        input.Interact += Interact;
        LockCursor();
    }
    
    private void OnDestroy()
    {
        input.Interact -= Interact;
    }

    private void Update()
    {
        CameraRotation();
        DetectInteractables();
    }

    private void CameraRotation()
    {
        var mouseInput = input.Input.Aim;
        mouseInput = Vector2.Scale(mouseInput, _smoothScale);

        //Interpolated value of mouse movement
        _smooth.x = Mathf.Lerp(_smooth.x, mouseInput.x, 1f / _settings.Smoothing);
        _smooth.y = Mathf.Lerp(_smooth.y, mouseInput.y, 1f / _settings.Smoothing);

        //Incrementally add to the camera look
        _mouseLook += _smooth;
        _mouseLook.y = Mathf.Clamp(_mouseLook.y, -90f, 90f);

        //Set the character and view rotation
        transform.localRotation = Quaternion.AngleAxis(-_mouseLook.y, Vector3.right);
        playerTransform.localRotation = Quaternion.AngleAxis(_mouseLook.x, playerTransform.up);
    }

    public void AddYaw(float delta)
    {
        _mouseLook.x += delta;
    }

    private void DetectInteractables()
    {
        var aimPosition = _playerCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 2f));
        if (Physics.Raycast(_playerCamera.transform.position, aimPosition - _playerCamera.transform.position, out var hit, _settings.InteractDistance, _settings.InteractableLayers))
        {
            var interactable = hit.collider.gameObject.GetComponent<Interactable>();
            
            if (!interactable)
            {
                lookedAtInteractable.OnLookEnd();
                lookedAtInteractable.Value = null;
                return;
            }

            if (lookedAtInteractable.Value == interactable)
            {
                lookedAtInteractable.OnLookStay();
                return;
            }

            lookedAtInteractable.SetValue(interactable);
            lookedAtInteractable.OnLookBegin();
        }
        else if (lookedAtInteractable)
        {
            lookedAtInteractable.OnLookEnd();
            lookedAtInteractable.Value = null;
        }
    }

    private void Interact(bool left)
    {
        lookedAtInteractable.OnInteract(left);
    }

    private static void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
}