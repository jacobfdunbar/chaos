﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerInputController : MonoBehaviour
{
    private InputInfo _currentInput = new InputInfo();
    public InputInfo Input => _currentInput;
    
    public Action<bool> Interact;
    public Action Jump;
    
    [UsedImplicitly]
    public void OnAim(InputAction.CallbackContext context)
    {
        _currentInput.Aim = context.ReadValue<Vector2>();
    }

    [UsedImplicitly]
    public void OnMovement(InputAction.CallbackContext context)
    {
        _currentInput.Movement = context.ReadValue<Vector2>();
    }

    [UsedImplicitly]
    public void OnSprint(InputAction.CallbackContext context)
    {
        _currentInput.Sprint = context.performed;
    }

    [UsedImplicitly]
    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.performed) Jump?.Invoke();
    }

    [UsedImplicitly]
    public void OnInteractLeft(InputAction.CallbackContext context)
    {
        if (context.performed) Interact?.Invoke(true);
    }

    [UsedImplicitly]
    public void OnInteractRight(InputAction.CallbackContext context)
    {
        if (context.performed) Interact?.Invoke(false);
    }

    [UsedImplicitly]
    public void OnReset(InputAction.CallbackContext context)
    {
        if (context.performed) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    [Serializable]
    public struct InputInfo
    {
        public Vector2 Aim;
        public Vector2 Movement;
        public bool Sprint;
    }
}