﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerInputController))]
public class PlayerMovementController : PortalTraveller
{
    [Header("Camera Reference")]
    [SerializeField] private PlayerCameraController playerCamera = null;

    [Header("Movement Settings")]
    [SerializeField] private MovementSettings settings = null;

    private CharacterController _controller = null;
    private PlayerInputController _input = null;
    
    private bool _isJumping = false;
    private float _movementSpeed;

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
        
        _input = GetComponent<PlayerInputController>();
        _input.Jump += Jump;

        _movementSpeed = settings.WalkSpeed;
    }

    private void OnDestroy()
    {
        _input.Jump -= Jump;
    }

    public void Update()
    {
        PlayerMovement();
    }

    private void PlayerMovement()
    {
        var vertInput = _input.Input.Movement.y;
        var horizInput = _input.Input.Movement.x;

        var longitudinalMovement = _controller.transform.forward * vertInput;
        var lateralMovement = _controller.transform.right * horizInput;
        
        //Applies Time.deltaTime under the hood
        _controller.SimpleMove(Vector3.ClampMagnitude(longitudinalMovement + lateralMovement, 1) * _movementSpeed);

        if ((Math.Abs(vertInput) > 0.0001f || Math.Abs(horizInput) > 0.0001f) && OnSlope())
        {
            _controller.Move(Vector3.down * _controller.height / 2 * (settings.SlopeForce * Time.deltaTime));
        }

        UpdateMovementSpeed();
    }

    private void UpdateMovementSpeed()
    {
        _movementSpeed = Mathf.MoveTowards(
            _movementSpeed, 
            _input.Input.Sprint ? settings.RunSpeed : settings.WalkSpeed,
            settings.SpeedDifference * (Time.deltaTime / settings.RunBuildUpTime));
    }

    private bool OnSlope()
    {
        if (_isJumping) return false;

        if (!Physics.Raycast(_controller.transform.position, Vector3.down, out var hit,
            (_controller.height / 2) + settings.SlopeForceRayLength)) return false;
        return hit.normal != Vector3.up;
    }

    private void Jump()
    {
        if (!settings.CanJump || _isJumping) return;
        
        _isJumping = true;
        StartCoroutine(JumpEvent());
    }

    private IEnumerator JumpEvent()
    {
        _controller.slopeLimit = 90;
        var timeInAir = 0.0f;

        do
        {
            var jumpForce = settings.JumpFallOff.Evaluate(timeInAir);
            _controller.Move(Vector3.up * (jumpForce * settings.JumpMultiplier * Time.deltaTime));
            timeInAir += Time.deltaTime;
            yield return null;
        } while (!_controller.isGrounded && _controller.collisionFlags != CollisionFlags.Above);

        _controller.slopeLimit = 45;
        _isJumping = false;
    }

    public override void Teleport(Transform fromPortal, Transform toPortal, Vector3 pos, Quaternion rot)
    {
        transform.position = pos;
        var delta = Mathf.DeltaAngle (transform.eulerAngles.y, rot.eulerAngles.y);
        playerCamera.AddYaw(delta);
        Physics.SyncTransforms ();
    }
}
