﻿using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    public static T Instance { get; protected set; } = default(T);

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            Debug.LogErrorFormat("An instance of {0} already exists.", typeof(T).ToString());
            return;
        }
        Instance = (T)this;
    }

    protected virtual void OnBecameInstance() {}
}