using UnityEngine;

public class HandMattersDoor : Door
{
    [SerializeField] private Room leftHandRoom = null;
    [SerializeField] private Room rightHandRoom = null;
    
    protected override void HandleDoorOpen(bool left)
    {
        OpenToRoom(left ? leftHandRoom : rightHandRoom);
    }
}
