using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorAnimator : MonoBehaviour
{
    public enum AnimationType
    {
        Default = -1,
        Towards,
        Away,
        Slide,
        Scale,
        Fall,
        Vacuumed
    }

    [SerializeField] private AnimationType _animationType = default;
    
    private Animator _animator;
    private static readonly int CloseTrigger = Animator.StringToHash("Close");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Open()
    {
        _animator.SetTrigger(_animationType.ToString());
    }

    public void Open(AnimationType animationType)
    {
        _animator.SetTrigger(animationType.ToString());
    }

    public void Close()
    {
        _animator.SetTrigger(CloseTrigger);
    }
}