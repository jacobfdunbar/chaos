using UnityEngine;

[RequireComponent(typeof(DoorAnimator))]
public abstract class Door : Interactable
{
    [SerializeField] private bool canBeOpenedManually = true;

    private DoorAnimator _animator;
    private bool _hasBeenOpened = false;
    
    private void Awake()
    {
        _animator = GetComponent<DoorAnimator>();
    }

    public override void OnInteract(bool left)
    {
        if (!canBeOpenedManually || _hasBeenOpened) return;
        HandleDoorOpen(left);
    }

    protected abstract void HandleDoorOpen(bool left);

    public virtual void ForceOpen()
    {
        OpenToRoom(null);
    }

    protected void OpenToRoom(Room room, DoorAnimator.AnimationType animationType = DoorAnimator.AnimationType.Default)
    {
        if (_hasBeenOpened) return;
        _hasBeenOpened = true;

        var interactionCollider = GetComponent<Collider>();
        if (interactionCollider) interactionCollider.enabled = false;
        
        if (room)
        {
            var spawnPosition = transform.position + Vector3.forward * 0.5f;
            var spawnedRoom = Instantiate(room, spawnPosition, Quaternion.identity);
            if (spawnedRoom)
            {
                animationType = spawnedRoom.AnimationOverride;
                spawnedRoom.EntranceDoor = this;
            }
        }

        if (animationType == DoorAnimator.AnimationType.Default)
        {
            _animator.Open();
        }
        else
        {
            _animator.Open(animationType);
        }
    }

    public void Close()
    {
        _animator.Close();
    }
}
