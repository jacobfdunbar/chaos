using UnityEngine;

public class RegularDoor : Door
{
    [SerializeField] private Room nextRoom = null;
    
    protected override void HandleDoorOpen(bool left)
    {
        OpenToRoom(nextRoom);
    }

    public override void ForceOpen()
    {
        OpenToRoom(nextRoom);
    }
}