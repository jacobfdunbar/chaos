﻿using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    private static readonly List<Portal> Portals = new List<Portal>();

    public static void RegisterPortal(Portal portal)
    {
        Portals.Add(portal);
    }

    public static void UnregisterPortal(Portal portal)
    {
        Portals.Remove(portal);
    }

    private void OnPreCull()
    {
        Portals.ForEach(portal => portal.PrePortalRender());
        Portals.ForEach(portal => portal.Render());
        Portals.ForEach(portal => portal.PostPortalRender());
    }
}